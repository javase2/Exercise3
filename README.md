# Exercise3
Создание классов

#Опция "public static void main(String[] args)" для "OnlineMedia.java"

#начало копирования

public class OnlineMedia {

	public static void main(String[] args) {
		
	}
}

#конец копирования


#Содержание файла "DigitalVideoDisс.java"

public class DigitalVideoDisc {

}

#Содержание файла "Order.java"

public class Order {

}

#Содержание файла "DataFromProperties.java"

public class DataFromProperties {

}

#Скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build",
            "command": "mvn clean install",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise3",
                "component": "maven"
            },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "mvn clean install && java -jar ./target/*.jar",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise3",
                "component": "maven"
             },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia test",
            "command": "mvn verify",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise3",
                "component": "maven"
             },
            "problemMatcher": []
        }
    ]
}
